// console.log("Hello World");

// While Loop
// let count = 5;

// while (count !== 0) {
//     console.log(`While: ${count}`);

//     count--;
// }

// Do While Loop
// run the code atleast once
// let number = Number(prompt("Give me a number!"));

// do {
//     console.log(`Do While: ${number}`);

//     number--;
// } while (number !== 0);

// For Loop
// for (let count = 0; count <= 20; count++) {
//     console.log(`The current value of count is: ${count}`);
// }

// let myString = "alex";
// for (let index = 0; index < myString.length; index++) {
//     console.log(`${myString[index]}`); 
// }

// let numA = 15;
// for (let index = 0; index <= 5; index++) {
//     let exponential = numA ** index;
//     console.log(exponential);
// }

// Create a String name "myName" with value of "Alex"
// let myName = "Alex";

// for (let index = 0; index < myName.length; index++) {
//     myName = myName.toLowerCase();
//     if (myName[index] === 'a' || myName[index] === 'e' || myName[index] === 'i' || myName[index] === 'o' || myName[index] === 'u' ) {
//         console.log(3);
//     }
//     else {
//         console.log(myName[index]);
//     }
// }

// Continue and Break statements
// for (let count = 0; count <= 20; count++) {
//     if (count % 2 == 0) {
//         console.log(`${count}`);
//         continue;
//     } 
//     else if(count > 10){
//         break;
//     }

// }

let name = "alexandro";

for (let i = 0; i < name.length; i++) {
    if(name[i].toLowerCase() === "a"){
        console.log(`Continue to next iteration`);
        continue;
    }
    if(name[i].toLowerCase() === "d"){
        console.log(`Console log before the break!`);
        break;
    }
    console.log(name[i]);
}